import time
import signal
import logging

from crypto import Crypto


def main():
    # Handle the signal to stop the Docker container.
    signal.signal(signal.SIGTERM, handle_sigterm)
    logging.basicConfig(level=logging.INFO, format="%(asctime)s %(message)s")

    crypto: Crypto = None
    try:
        crypto = init_crypto()
        setup_candlestick_makers(crypto)
        start_data_fetching(crypto)
    except Exception as error:
        if crypto and crypto.is_running():
            print("Stopping data fetching...")
            crypto.stop()

        print(error)


def handle_sigterm(*args):
    raise KeyboardInterrupt()


def init_crypto() -> Crypto:
    try:
        print("Connecting to MySQL...")
        database_url = "mysql://crypto:1234@mysql/crypto"
        crypto = Crypto(database_url)
        return crypto
    except KeyboardInterrupt:
        raise Exception("Keyboard interrupt.")
    except Exception as error:
        print(error)
        raise Exception("Couldn't connect to MySQL.")


def setup_candlestick_makers(crypto: Crypto):
    print("Setting up candlestick makers...")
    try:
        crypto.set_currencies_to_make_candlesticks(
            [
                ("USDC_BTC", 60),
                ("USDC_BTC", 300),
                ("USDC_BTC", 600),
                ("USDC_ETH", 60),
                ("USDC_ETH", 300),
                ("USDC_ETH", 600),
            ]
        )
    except KeyboardInterrupt:
        raise Exception("Keyboard interrupt.")
    except Exception:
        raise Exception("Couldn't setup candlestick makers.")


def start_data_fetching(crypto: Crypto):
    print("Starting data fetching...")
    try:
        crypto.start()

        # Healthcheck.
        healthcheck_interval_in_seconds = 30
        while True:
            print(f"Crypto is running: {crypto.is_running()}")
            time.sleep(healthcheck_interval_in_seconds)

    except KeyboardInterrupt:
        raise Exception("Keyboard interrupt.")
    except Exception:
        raise Exception("Couldn't start data fetching.")


main()
