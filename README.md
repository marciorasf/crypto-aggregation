# Crypto

## Desafio

O desafio consistiu em construir um sistema que consuma dados de cotações de criptomoedas em tempo real da API do Poloniex e construa candlesticks baseados nesses dados. Devem ser construídos candlesticks com intervalo de 1, 5 e 10 minutos para uma criptomoeda, sendo que o suporte a mais de uma criptomoeda seria considerado um extra.

## Descrição

Foi construído o sistema proposto no desafio, com suporte às criptomoedas listadas em [PoloniexAPI](https://docs.poloniex.com/#currency-pair-ids) (exceto TRX_KLV e USDT_QUICK, por conta de duplicação dos IDs) e candlesticks de 1, 5 e 10 minutos. Contudo, pode ser facilmente modificado para que os candlesticks possam ter intervalos arbitrários. Os valores foram restringidos apenas para evitar erros causados por valores de intervalos não testados.

## Execução para avaliação

- Requisitos: Docker e DockerCompose.
  
- Clonar o repositório.
  
- Com o terminal na raiz da pasta do projeto, executar: `docker-compose up`.
  
- Alguns logs aparecerão no terminal em que foi executado o comando acima contendo algumas informações sobre a inicialização da aplicação. Sempre que um novo candlestick for construído e salvo no banco, uma mensagem também será impressa no terminal com as informações suas informações.
  
- Uma instância do **Grafana** ficará disponível em http://localhost:3000, nela terá uma dashboard com nome **Candlestick** que contém tabelas mostrando os candlesticks construídos pela aplicação. O link direto para a dashboard é http://localhost:3000/d/vGthVAW7z/candlestick?orgId=1&refresh=5s (fiz testes executando o projeto do zero e o endereço se manteve inalterado, mas pode ser que mude. Nesse caso teria que acessar a página inicial do Grafana e escolher a dashboard manualmente).
  
- Você também pode acessar o banco de dados através do **Adminer** que ficará disponível em http://localhost:8080. Os dados para acesso são:
    - Servidor: mysql
    - Usuário: crypto
    - Senha: 1234
    - Base de dados: crypto

## Utilização

Para começar a utilização, você deve primeiro importar a classe Crypto:

```python
from crypto import Crypto
```

Depois é preciso instanciar um objeto Crypto passando a url de um banco de dados MySQL:

```python
database_url = "mysql://crypto:1234@mysql/crypto"
crypto = Crypto(database_url) 
```

Antes de iniciar a construção dos candlesticks, é necessário configurar as moedas e intervalos de tempo de cada candlestick desejado. Para isso deve-se chamar o método **set_currencies_to_make_candlesticks(currency_periodicity_tuples)** passando uma lista de tuplas com o seguinte formato: 

(MOEDA, PERIODICIDADE_EM_SEGUNDOS)

MOEDA = string representando algum dos "Currency Pair" listados em [PoloniexAPI](https://docs.poloniex.com/#currency-pair-ids) (exceto TRX_KLV e USDT_QUICK, por conta de duplicação dos IDs).

PERIODICIDADE_EM_SEGUNDOS = valor inteiro contido no conjunto [60, 300, 600].

```python
crypto.set_currencies_to_make_candles(
    [
        ("USDC_BTC", 60),
        ("USDC_BTC", 300),
        ("USDC_ETH", 60),
        ("USDC_ETH", 300),
    ]
)
```

Agora basta iniciar o processo de busca de informações na Poloniex e construção de candlesticks:

```python
crypto.start()
```

A qualquer momento, você pode conferir se o processo de construção de candlesticks está em andamento com o método **is_running()** ou parar esse processo com o método **stop()**:

```python
crypto.is_running()
crypto.stop()
```

Por fim, você pode utilizar o método **retrieve_candlesticks(currency_pair_name, periodicity_in_seconds, after_timestamp=0, before_timestamp=0)** para buscar os candlesticks no banco de dados:

```python
import time

one_hour_ago_timestamp = time.time() - 60*60
crypto.retrieve_candlesticks("USDC_BTC", 60, one_hour_ago_timestamp)
```

### Logging

A biblioteca registra os eventos de criação de candlestick usando o logging. Para pode ver os eventos gerados, basta habilitar o logging:

```python
import logging

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(message)s")
```

### Exemplo

```python
import time
import logging

from crypto import Crypto

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(message)s")

database_url = "mysql://crypto:1234@mysql/crypto"
crypto = Crypto(database_url) 
crypto.set_currencies_to_make_candles(
    [
        ("USDC_BTC", 60),
        ("USDC_BTC", 300),
        ("USDC_ETH", 60),
        ("USDC_ETH", 300),
    ]
) 

crypto.start()
time.sleep(2400) # Wait 20 minutes before stopping.
crypto.stop()
```

Outro exemplo de utilização da API pode ser vista no arquivo [client.py](client.py).

## Tecnologias e Ferramentas

- [Python 3.9.6](https://www.python.org/)
- [Function Annotations](https://www.python.org/dev/peps/pep-3107/)
- [peewee](http://docs.peewee-orm.com/en/latest/)
- [websocket-client](https://websocket-client.readthedocs.io/en/latest/)
- [pytest](https://docs.pytest.org/en/6.2.x/)
- [pytest-mock](https://pypi.org/project/pytest-mock/)
- [MySQL 8.0.26](https://www.mysql.com/)
- [Git](https://git-scm.com/)
- [GitLab](https://gitlab.com/)
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
- [Docker](https://www.docker.com/)
- [Adminer](https://www.adminer.org/)
- [Grafana](https://grafana.com/)

## Implementação

A arquitetura foi inspirada primeiramente em um modelo Publish/Subscribe, onde teria um **producer** responsável por buscar os dados do Poloniex e publicar as atualizações por tópicos (criptomoedas) e os **subscribers** seriam serviços que reagiriam à essas atualizações para construir candlesticks.

Para manter o projeto simples, implementei uma ideia similar ao Pub/Sub mas usando o padrão Observer. Nesse caso existe um gerador de atualizações **DataFetcher** que é responsável por buscar as informações do Poloniex via websocket e informar aos objetos observadores (que implemetam a interface **DataFetcherObserver**) sobre as atualizações das cotações das criptomoedas. Uma modificação importante em relação ao padrão Observer básico, é que os objetos do tipo **DataFetcher**, guardam os objetos **DataFetcherObserver** em um dicionário de listas, onde a chave para cada lista é o nome de uma criptomoeda, dessa forma apenas os **DataFetcherObserver** interessados na moeda atualizada da vez serão informados da atualização.

Como escrito acima a conexão via websocket e recebimento dos dados da Poloniex são responsabilidades da class **DataFetcher**.Para não bloquear a execução do restante do sistema enquanto o websocket está rodando, optei por colocá-lo em uma thread separada. Como ele não modifica variáveis externas, acredito que não gerará problemas de concorrência.

A classe que implementa a interface **DataFetcherObserver** e é responsável por construir os candlesticks a partir das atualizações de cotações é a **CandlestickMaker**. Os candlesticks são salvos no banco utilizando a classe **Candlestick** que é criada de acordo com o ORM **peewee** para conseguir interagir com o banco de dados de forma mais prática.

Por fim decidir criar a classe **Crypto** para especificar a API do sistema, mantendo-a objetiva e limpa, e também para poupar os clientes dos detalhes de implementação.

### Banco de dados

Foi utilizado o ORM [peewee](http://docs.peewee-orm.com/en/latest/) para facilitar a manipulação dados no banco de dados.

Durante o desenvolvimento foi utilizado um banco MySQL, mas outros bancos relacionados podem ser utilizados, desde que sejam compatíveis com o peewee e que as bibliotecas adicionais sejam instaladas.

O banco de dados possui uma única tabela **candlestick** com as seguintes colunas:

- currency_pair_name: nome da criptomoeda.
- periodicity_in_seconds: a periodicidade escolhida para o candlestick.
- actual_periodicity_in_seconds: intervalo real do candlestick. Como as atualizações não acontecem no tempo exato para coincidir com a periodicidade configurada, escolhi salvar esse dado também.
- open_timestamp: timestamp de ínicio da construção do candlestick.
- close_timestamp: timestamp de fechamento da construção do candlestick.
- open_price: preço de abertura.
- close_price: preço de fechamento.
- low_price: menor preço durante o intervalo do candlestick.
- high_price: maior preço durante o intervalo do candlestick.

### Versionamento de código

O código foi versionado com [git](https://git-scm.com/) com um [repositório remoto](https://gitlab.com/marciorasf/crypto-aggregation) no GitLab. Para melhorar a padronização foram utilizados os formatadores [black](https://github.com/psf/black) e [flake8](https://flake8.pycqa.org/en/latest/) que são executados automaticamente durante o estágio de pre-commit. Para assegurar a padronização dos commits, foi utilizado o [commitizen](https://github.com/commitizen/cz-cli), que também é executado durante o pre-commit.

### Testes

Para os testes foi usado o [pytest](https://docs.pytest.org/en/6.2.x/) e o [pytest-mock](https://pypi.org/project/pytest-mock/).

### CI/CD

Foi utilizado o [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) para executar os testes após o `push` das alterações. Um pipeline de CI/CD mais elaborado pode ser construído a partir deste contendo testes de integração, testes end-to-end e publicação da biblioteca no PyPI.

## Melhorias

- Caso o número de criptomoedas e o número de diferentes intervalos para cada uma fosse grande, talvez fosse interessante alterar a implementação para utilizar realmente um Publish/Subscribe, transformando o DataFetcher em publisher e os vários CandlestickMakers em subscribers.
  
- Escrever testes que garantam que mudanças no websocket da Poloniex que quebrem o funcionamento do código sejam detectadas.
  
- Melhorar o tratamento e lançamento de erros. Os erros ainda estão muito genéricos. Um erro muito importante, que representa um ponto único de falha, que deve ser tratado é a desconexão do websocket, que atualmente não lança nenhuma exceção nem faz uma tentativa automática de reconexão.

- Melhorar os nomes usados no código para nomes mais próximos do domínio da aplicação.
  
- Não tenho muito conhecimento sobre como são usadas as docstrings dentro de bibliotecas Python. Por isso escolhi me guiar pelos princípios descritos no [Clean Code]( https://www.amazon.com.br/C%C3%B3digo-limpo-Robert-C-Martin/dp/8576082675/ref=asc_df_8576082675/?tag=googleshopp00-20&linkCode=df0&hvadid=379792215563&hvpos=&hvnetw=g&hvrand=4220224890334237518&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=1001566&hvtargid=pla-398225630878&psc=1 ), mantendo os comentários apenas onde não consegui me expressar de forma clara apenas pelo código, ou quando considerei importante explicar alguma decisão tomada.

## Desafios

- Decidir se faria utilizando a API do Poloniex utilizando requisições HTTP ou com websocket. Escolhi fazer com websocket pois considerei que a aplicação era mais compatível com a utilização de websockets visto que se trata de um fluxo contínuo de atualização das cotações das moedas. Contudo, para manter essa decisão aberta mudanças, declarei uma interface abstrata **DataFetcher** com os métodos que deveriam ser implementados, e caso eu quisesse trocar para usar requisições HTTP, bastaria criar uma classe que implementasse essa mesma interface e trocar nos locais de instanciação.
  
- Como deixar o websocket rodando sem bloquear o restante da aplicação? Decidi deixar rodando em uma thread, pesquisei um pouco sobre como fazer isso, e consegui manter o websocket rodando dentro de uma thread sem bloquear o restante da aplicação.
  
- Como escrever os testes que envolvem as chamadas à Poloniex? Acredito que implementar um Test Double que simule o funcionamento da comunicação via websocket com a Poloniex, ajude bastante no desenvolvimento de testes que envolvam à Poloniex e também a reduzir consideravelmente o tempo necessário para executar esses testes.

- Qual a API ideal para biblioteca? Aqui eu decidi por manter uma interface mínima que contemplasse os requisitos teste técnico, mas que também fizesse sentido para um outro cliente qualquer. Acredito que caso essa biblioteca fosse levada adiante, surgiriam novas necessidades na API, que poderiam ser implementadas, por isso tentei sempre manter a flexibilidade para extensão da API.

- Qual a melhor forma de mostrar o trabalho realizado para os avaliadores? Decidi configurar um container com o Grafana, com algumas dashboards configurados para visualização dos dados obtidos durante o teste. Decidi também fazer imprimir no console o log de algumas ações realizadas, como "Connecting to MySQL", "NEW CANDLESTICK: <dados>".