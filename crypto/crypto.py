import time
from typing import List, Tuple

from crypto.candlestick.candlestick import Candlestick
from crypto.candlestick.candlestick_maker import CandlestickMakerImp
from crypto.data_fetcher.data_fetcher import DataFetcherImp
from crypto.setup import setup


class Crypto:
    def __init__(self, database_url: str) -> None:
        setup(database_url)
        self._data_fetcher = DataFetcherImp()

    def set_currencies_to_make_candlesticks(self, currency_periodicity_tuples: List[Tuple[str, int]]) -> None:
        """Configure the candlesticks that will be built.

        Args:
        currency_periodicity_tuples: a list of tuples, each tuple containing (currency_pair_name, periodicity_in_seconds). currency_pair_name accepts the values displayed in https://docs.poloniex.com/#currencies and periodicity_in_seconds accepts one of the following values: 60, 300, 600.
        """
        for currency_pair_name, periodicity_in_seconds in currency_periodicity_tuples:
            observer = CandlestickMakerImp(currency_pair_name, periodicity_in_seconds)
            self._data_fetcher.attach_observer(observer)

    def start(self) -> None:
        self._data_fetcher.start()

    def stop(self) -> None:
        self._data_fetcher.stop()

    def is_running(self) -> None:
        return self._data_fetcher.is_running()

    def retrieve_candlesticks(
        self,
        currency_pair_name: str,
        periodicity_in_seconds: int,
        after_timestamp: float = 0.0,
        before_timestamp: float = time.time(),
    ) -> List[Candlestick]:
        return list(
            Candlestick.select().where(
                (Candlestick.currency_pair_name == currency_pair_name)
                & (Candlestick.periodicity_in_seconds == periodicity_in_seconds)
                & (Candlestick.open_timestamp > after_timestamp)
                & (Candlestick.open_timestamp < before_timestamp)
            )
        )
