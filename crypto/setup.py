import logging
import time

from playhouse.db_url import connect

from crypto.candlestick.candlestick import Candlestick
from crypto.database import database

MAX_CONNECTION_ATTEMPTS = 10
ATTEMPT_INTERVAL_IN_SECONDS = 10


def setup(database_url: str) -> None:
    connected = False
    connect_attempts = 0

    while connect_attempts < MAX_CONNECTION_ATTEMPTS and not connected:
        try:
            database.initialize(connect(database_url))
            with database:
                database.create_tables([Candlestick])

            connected = True
        except Exception:
            if connect_attempts < MAX_CONNECTION_ATTEMPTS - 1:
                logging.warning(f"Couldn't connect to database. Trying again in {ATTEMPT_INTERVAL_IN_SECONDS} seconds.")

                time.sleep(ATTEMPT_INTERVAL_IN_SECONDS)

            connect_attempts += 1

    if not connected:
        logging.fatal("Couldn't connect to database.")
        raise RuntimeError("Couldn't connect to database.")
