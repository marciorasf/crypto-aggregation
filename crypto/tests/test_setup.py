import pytest
from crypto import setup
from pytest_mock.plugin import MockerFixture


def test_it_should_raise_runtime_error_if_invalid_database(mocker: MockerFixture):
    mocker.patch.object(setup, "MAX_CONNECTION_ATTEMPTS", 1)

    with pytest.raises(RuntimeError):
        setup.setup("wrong_database_url")


def test_it_should_try_n_times_before_raising_error(mocker: MockerFixture):
    N_ATTEMPTS = 10
    mocker.patch.object(setup, "ATTEMPT_INTERVAL_IN_SECONDS", 0)
    spy = mocker.spy(setup, "setup")

    with pytest.raises(RuntimeError):
        setup.setup("wrong_database_url")
        assert spy.call_count == N_ATTEMPTS
