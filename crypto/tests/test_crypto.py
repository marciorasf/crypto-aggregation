import time

import pytest
from crypto import Crypto


@pytest.mark.skip(reason="it depends on network calls so it takes a lot of time to test everytime")
def test_crypto():
    database_url = "mysql://crypto:1234@localhost:3306/crypto"
    crypto = Crypto(database_url)
    crypto.set_currencies_to_make_candlesticks(
        [
            ("USDC_BTC", 60),
            ("USDC_BTC", 300),
            ("USDC_BTC", 600),
            ("USDC_ETH", 60),
            ("USDC_ETH", 600),
        ]
    )

    start_timestamp = time.time()
    crypto.start()
    time_running_in_seconds = 60 * 25
    time.sleep(time_running_in_seconds)
    crypto.stop()

    expected_60_seconds_candlesticks = time_running_in_seconds // 60
    btc_60_seconds = crypto.retrieve_candlesticks("USDC_BTC", 60, start_timestamp)
    eth_60_seconds = crypto.retrieve_candlesticks("USDC_ETH", 60, start_timestamp)
    assert len(btc_60_seconds) == pytest.approx(expected_60_seconds_candlesticks, 1)
    assert len(eth_60_seconds) == pytest.approx(expected_60_seconds_candlesticks, 1)

    expected_300_seconds_candlesticks = time_running_in_seconds // 300
    btc_300_seconds = crypto.retrieve_candlesticks("USDC_BTC", 300, start_timestamp)
    eth_300_seconds = crypto.retrieve_candlesticks("USDC_ETH", 300, start_timestamp)
    assert len(btc_300_seconds) == pytest.approx(expected_300_seconds_candlesticks, 1)
    assert len(eth_300_seconds) == pytest.approx(expected_300_seconds_candlesticks, 1)

    expected_600_seconds_candlesticks = time_running_in_seconds // 600
    btc_600_seconds = crypto.retrieve_candlesticks("USDC_BTC", 600, start_timestamp)
    eth_600_seconds = crypto.retrieve_candlesticks("USDC_ETH", 600, start_timestamp)
    assert len(btc_600_seconds) == pytest.approx(expected_600_seconds_candlesticks, 1)
    assert len(eth_600_seconds) == pytest.approx(expected_600_seconds_candlesticks, 1)
