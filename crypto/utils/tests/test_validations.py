from crypto.utils import validations
import pytest


def test_it_should_raise_exception_if_periodicity_in_seconds_is_string():
    with pytest.raises(TypeError):
        validations.validate_periodicity_in_seconds("60")


def test_it_should_raise_exception_if_periodicity_in_seconds_is_not_expected_value():
    with pytest.raises(ValueError):
        validations.validate_periodicity_in_seconds(50)


def test_it_should_do_nothing_if_periodicity_in_seconds_is_valid():
    validations.validate_periodicity_in_seconds(60)
    assert True


def test_is_should_raise_exception_if_currency_pair_name_is_unknown():
    with pytest.raises(KeyError):
        validations.validate_currency_pair_name("BTC")


def test_is_should_do_nothing_if_currency_pair_name_is_valid():
    validations.validate_currency_pair_name("BNB_BTC")
    assert True
