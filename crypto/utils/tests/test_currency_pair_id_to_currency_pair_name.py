from crypto.utils.currency_pair import currency_pair_id_to_currency_pair_name
import pytest


def test_it_should_return_BNB_BTC():
    currency_pair_id = 336

    currency_pair_name = currency_pair_id_to_currency_pair_name(currency_pair_id)

    assert currency_pair_name == "BNB_BTC"


def test_it_should_return_BNB_BTC_even_if_currency_pair_id_is_string():
    currency_pair_id = "336"

    currency_pair_name = currency_pair_id_to_currency_pair_name(currency_pair_id)

    assert currency_pair_name == "BNB_BTC"


def test_it_should_raise_an_exception():
    currency_pair_id = 0

    with pytest.raises(KeyError):
        currency_pair_id_to_currency_pair_name(currency_pair_id)
