from typing import Union

from crypto.utils.currency_pair_map import CURRENCY_PAIR_ID_NAME_MAP


def currency_pair_id_to_currency_pair_name(currency_pair_id: Union[int, str]) -> str:
    key = int(currency_pair_id)
    if key in CURRENCY_PAIR_ID_NAME_MAP:
        return CURRENCY_PAIR_ID_NAME_MAP[key]

    raise KeyError("Unknown currency_pair_id.")
