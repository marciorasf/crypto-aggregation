from crypto.utils.currency_pair_map import CURRENCY_PAIR_ID_NAME_MAP


def validate_currency_pair_name(currency_pair_name: str) -> None:
    if currency_pair_name not in CURRENCY_PAIR_ID_NAME_MAP.values():
        raise KeyError(f"unknown currency_pair_name: {str(currency_pair_name)}.")


def validate_periodicity_in_seconds(periodicity_in_seconds: int) -> None:
    if not isinstance(periodicity_in_seconds, int):
        raise TypeError("periodicity_in_seconds must be integer.")

    valid_values = [60, 300, 600]
    if periodicity_in_seconds not in valid_values:
        raise ValueError(f"periodicity_in_seconds must be one of the following values: {', '.join(str(valid_values))}.")
