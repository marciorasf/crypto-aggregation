from crypto.data_fetcher.data_fetcher import CurrencyUpdate, DataFetcher, DataFetcherImp, DataFetcherObserver
import time
import pytest

data_fetcher_implementation: DataFetcher = DataFetcherImp


def test_it_should_not_be_running_after_initialization():
    data_fetcher = data_fetcher_implementation()

    assert not data_fetcher.is_running()


def test_it_should_be_running_after_start():
    data_fetcher = DataFetcherImp()

    data_fetcher.start()

    assert data_fetcher.is_running()

    data_fetcher.stop()


def test_it_should_not_be_running_after_stop():
    data_fetcher = data_fetcher_implementation()

    data_fetcher.start()
    data_fetcher.stop()

    assert not data_fetcher.is_running()


@pytest.mark.skip(reason="it depends on network calls so it takes a lot of time to test everytime")
def test_it_should_call_observer_update():
    class MockObserver(DataFetcherObserver):
        def __init__(self) -> None:
            self.update_was_called: bool = False

        def update(self, currency_data: CurrencyUpdate) -> None:
            self.update_was_called = True

        def get_currency_pair_name(self) -> str:
            return "BNB_BTC"

    data_fetcher = data_fetcher_implementation()

    observer = MockObserver()
    data_fetcher.attach_observer(observer)

    data_fetcher.start()
    seconds_to_wait = 60
    time.sleep(seconds_to_wait)
    data_fetcher.stop()

    assert observer.update_was_called


@pytest.mark.skip(reason="it depends on network calls so it takes a lot of time to test everytime")
def test_it_should_call_observer_with_currency_data():
    class MockObserver(DataFetcherObserver):
        def update(self, currency_data: CurrencyUpdate) -> None:
            if not isinstance(currency_data, CurrencyUpdate):
                raise Exception("NotCurrencyDataInstace")

        def get_currency_pair_name(self) -> str:
            return "BNB_BTC"

    data_fetcher = data_fetcher_implementation()

    observer = MockObserver()
    data_fetcher.attach_observer(observer)

    try:
        data_fetcher.start()
        seconds_to_wait = 60
        time.sleep(seconds_to_wait)
        data_fetcher.stop()
    except Exception:
        assert False

    assert True
