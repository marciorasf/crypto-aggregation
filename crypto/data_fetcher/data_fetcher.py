from __future__ import annotations

import _thread
import json
import time
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import List, Mapping

import websocket
from crypto.utils.currency_pair import currency_pair_id_to_currency_pair_name


@dataclass
class CurrencyUpdate:
    currency_pair_name: str
    last_trade_price: float
    timestamp: float


class DataFetcher(ABC):
    @abstractmethod
    def start(self) -> None:
        pass

    @abstractmethod
    def stop(self) -> None:
        pass

    @abstractmethod
    def is_running(self) -> bool:
        pass

    @abstractmethod
    def attach_observer(self, observer: DataFetcherObserver) -> None:
        pass

    @abstractmethod
    def detach_observer(self, observer: DataFetcherObserver) -> None:
        pass

    @abstractmethod
    def notify(self, currency_data: CurrencyUpdate) -> None:
        """Must notify only the observers with the same currency as on currency_data."""
        pass


class DataFetcherObserver(ABC):
    @abstractmethod
    def update(self, currency_data: CurrencyUpdate) -> None:
        pass

    @abstractmethod
    def get_currency_pair_name(self) -> str:
        pass


class DataFetcherImp(DataFetcher):
    def __init__(self) -> None:
        self._observers: Mapping[str, List[DataFetcherObserver]] = {}
        self._is_running: bool = False

    def start(self) -> None:
        if not self._is_running:
            self._start_fetcher()
            self._is_running = True

    def stop(self) -> None:
        self._is_running = False

    def is_running(self) -> bool:
        return self._is_running

    def attach_observer(self, observer: DataFetcherObserver) -> None:
        currency_pair_name = observer.get_currency_pair_name()
        if currency_pair_name in self._observers:
            self._observers[currency_pair_name].append(observer)
        else:
            self._observers[currency_pair_name] = [observer]

    def detach_observer(self, observer: DataFetcherObserver) -> None:
        currency_pair_name = observer.get_currency_pair_name()
        if currency_pair_name in self._observers:
            self._observers[currency_pair_name].remove(observer)

    def notify(self, currency_data: CurrencyUpdate) -> None:
        if currency_data.currency_pair_name in self._observers:
            for observer in self._observers[currency_data.currency_pair_name]:
                observer.update(currency_data)

    def _start_fetcher(self) -> None:
        def ws_thread(stop):
            uri = "wss://api2.poloniex.com"
            ws = websocket.WebSocket()
            ws.connect(uri)

            subscribe_payload = '{"command": "subscribe", "channel": 1002}'
            ws.send(subscribe_payload)

            acknowledgement = json.loads(ws.recv())
            if acknowledgement != [1002, 1]:
                raise Exception("Problem with acknowledgement")

            while True:
                if stop():
                    break
                message = json.loads(ws.recv())
                self._process_message(message)

            unsubscribe_payload = '{"command": "unsubscribe", "channel": 1002}'
            ws.send(unsubscribe_payload)
            ws.close()

        _thread.start_new_thread(ws_thread, (lambda: not self.is_running(),))

    def _process_message(self, message: List[str]) -> None:
        try:
            message_currency_data = message[2]
            currency_pair_name = currency_pair_id_to_currency_pair_name(message_currency_data[0])
            currency_data = CurrencyUpdate(currency_pair_name, float(message_currency_data[1]), time.time())
            self.notify(currency_data)
        except KeyError:
            self._react_to_unknown_currency_pair_id()

    def _react_to_unknown_currency_pair_id(self) -> None:
        pass
