import peewee
from crypto.database import database


class Candlestick(peewee.Model):
    currency_pair_name = peewee.CharField()
    periodicity_in_seconds = peewee.IntegerField()
    actual_periodicity_in_seconds = peewee.IntegerField()
    open_timestamp = peewee.TimestampField()
    close_timestamp = peewee.TimestampField()
    open_price = peewee.DoubleField()
    close_price = peewee.DoubleField()
    low_price = peewee.DoubleField()
    high_price = peewee.DoubleField()

    class Meta:
        database = database
