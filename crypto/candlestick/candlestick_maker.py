from __future__ import annotations

import logging
from abc import ABC, abstractmethod
from datetime import datetime

from crypto.candlestick.candlestick import Candlestick
from crypto.data_fetcher.data_fetcher import CurrencyUpdate, DataFetcherObserver
from crypto.utils import validations


class CandlestickMaker(DataFetcherObserver, ABC):
    @abstractmethod
    def __init__(self, currency_pair_name: str, periodicity_in_seconds: int) -> None:
        pass

    @abstractmethod
    def update(self, currency_data: CurrencyUpdate) -> None:
        pass

    @abstractmethod
    def get_currency_pair_name(self) -> str:
        pass


class CandlestickMakerImp(CandlestickMaker):
    def __init__(self, currency_pair_name: str, periodicity_in_seconds: int) -> None:
        validations.validate_periodicity_in_seconds(periodicity_in_seconds)

        self._currency_pair_name: str = currency_pair_name
        self._periodicity_in_seconds: int = periodicity_in_seconds
        self._open_timestamp: float = None
        self._close_timestamp: float = None
        self._open_price: float = None
        self._close_price: float = None
        self._low_price: float = None
        self._high_price: float = None

        self._logging = logging

    def update(self, currency_data: CurrencyUpdate) -> None:
        if not self._open_timestamp:
            # Runs on first update only.
            self._open_candlestick(currency_data)
        else:
            elapsed_time_since_open_in_seconds = int(currency_data.timestamp - self._open_timestamp)
            if elapsed_time_since_open_in_seconds > self._periodicity_in_seconds:
                self._close_candlestick()
                self._open_candlestick(currency_data)
            else:
                self._update_candlestick(currency_data)

    def get_currency_pair_name(self) -> str:
        return self._currency_pair_name

    def _open_candlestick(self, currency_data: CurrencyUpdate) -> None:
        self._open_timestamp = currency_data.timestamp
        self._close_timestamp = currency_data.timestamp
        self._open_price = currency_data.last_trade_price
        self._close_price = currency_data.last_trade_price
        self._low_price = currency_data.last_trade_price
        self._high_price = currency_data.last_trade_price

    def _update_candlestick(self, currency_data: CurrencyUpdate) -> None:
        self._close_timestamp = currency_data.timestamp
        self._close_price = currency_data.last_trade_price
        self._low_price = min(self._low_price, currency_data.last_trade_price)
        self._high_price = max(self._high_price, currency_data.last_trade_price)

    def _close_candlestick(self) -> None:
        candlestick = Candlestick.create(
            currency_pair_name=self._currency_pair_name,
            periodicity_in_seconds=self._periodicity_in_seconds,
            actual_periodicity_in_seconds=int(self._close_timestamp - self._open_timestamp),
            open_timestamp=self._open_timestamp,
            close_timestamp=self._close_timestamp,
            open_price=self._open_price,
            close_price=self._close_price,
            low_price=self._low_price,
            high_price=self._high_price,
        )
        candlestick.save()
        self._on_candlestick_close(candlestick)

    def _on_candlestick_close(self, candlestick: Candlestick) -> None:
        if self._logging:
            logging.info(
                f"NEW CANDLESTICK: currency_pair_name: {candlestick.currency_pair_name}, periodicity_in_seconds: {candlestick.periodicity_in_seconds}, open_time: {datetime.fromtimestamp(candlestick.open_timestamp)}, open_price:{candlestick.open_price}, close_price: {candlestick.close_price}, low_price: {candlestick.low_price}, high_price: {candlestick.high_price}."
            )
