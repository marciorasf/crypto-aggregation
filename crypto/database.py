import peewee

# Create a proxy that enables the use of the database object until the client specify the correct database
database = peewee.DatabaseProxy()
